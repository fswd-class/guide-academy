function FormSubmission() {
    event.preventDefault();
    const name = document.getElementById("name").value;
    const district = document.getElementById("district").value;
    const course = document.getElementById("course").value;
    const whatsappNo = document.getElementById("whatsappNo").value;
    const qualification = document.getElementById("qualification").value;
    const date = document.getElementById("inputDate").querySelector('input').value;
    const address = document.getElementById("address").value;
    const pinCode = document.getElementById("pinCode").value;
    const currentYear = new Date().getFullYear();


    if (!name || !district || !course || !whatsappNo || !qualification || !date || !address || !pinCode) {
        alert("Please fill out all fields.");
        return;
    }

    const courses = [
        { name: "Montessori TTC", courseFee: "20,000", admissionFee: "7,500" },
        { name: "Pre-Primary TTC", courseFee: "12,000", admissionFee: "5,000" },
        { name: "Arabic TTC", courseFee: "11,000", admissionFee: "4,500" },
        { name: "Hindi TTC", courseFee: "15,000", admissionFee: "5,000" },
        { name: "Diploma in Fashion Designing", courseFee: "4,000", admissionFee: "4,000" },
        { name: "Diploma in Beautician", courseFee: "3,500", admissionFee: "3,500" },
        { name: "Pharmacy Sales Assistant", courseFee: "10,000", admissionFee: "4,000" },
        { name: "Hospital Administration", courseFee: "18,000", admissionFee: "6,000" },
        { name: "Accounting", courseFee: "13,000", admissionFee: "5,000" },
        { name: "Degree", courseFee: "", admissionFee: "" },
        { name: "PG", courseFee: "", admissionFee: "" },
        { name: "SSLC (NIOS)", courseFee: "9,000", admissionFee: "4,000" },
        { name: "Plus Two (NIOS)", courseFee: "4,000", admissionFee: "4,000" },
        { name: "Plus Two (Jamia)", courseFee: "18,000", admissionFee: "18,000" },
        { name: "Spoken English", courseFee: "1,500", admissionFee: "1,500" },
        { name: "Jewellery Making", courseFee: "2,000", admissionFee: "2,000" },
        { name: "Bridal Makeup", courseFee: "3,500", admissionFee: "3,500" },
        { name: "Diploma in Acupuncture", courseFee: "", admissionFee: "" },
        { name: "Stitching Course", courseFee: "1,000", admissionFee: "1,000" },
        { name: "Home Tuition", courseFee: "3,500", admissionFee: "3,500" },
        { name: "Abacus", courseFee: "6,900", admissionFee: "6,900" }
    ];

    const selectedCourse = courses.find(c => c.name === course);

    if (!selectedCourse) {
        alert("Selected course not found in the list.");
        return;
    }

    const admissionFee = selectedCourse.admissionFee;
    const courseFee = selectedCourse.courseFee;

    const url = "https://wa.me/919747436459?text="
        + encodeURIComponent("*_Guide Academy Admission Form_" + currentYear + "_*") + "%0a%0a"
        + "Name: " + encodeURIComponent(name) + "%0a"
        + "Address: " + encodeURIComponent(address) + "%0a"
        + "Pin Code: " + encodeURIComponent(pinCode) + "%0a"
        + "District: " + encodeURIComponent(district) + "%0a"
        + "Course: " + encodeURIComponent(course) + "%0a"
        + "Whatsapp No: " + encodeURIComponent(whatsappNo) + "%0a"
        + "Qualification: " + encodeURIComponent(qualification) + "%0a"
        + "Date: " + encodeURIComponent(date) + "%0a"
        + "Course Fee: " + encodeURIComponent(courseFee) + "%0a"
        + "Admission Fee: " + encodeURIComponent(admissionFee);

    window.open(url, '_blank').focus();
}


document.getElementById('whatsappNo').addEventListener('input', function(event) {
    let inputValue = event.target.value;

    event.target.value = inputValue.replace(/\D/g, '');
});

document.getElementById('pinCode').addEventListener('input', function(event) {
    let inputValue = event.target.value;

    event.target.value = inputValue.replace(/\D/g, '');
});
